#!/bin/sh
# Here are some parameters. See all on
# https://pgbouncer.github.io/config.html

PG_LOG=/var/log/pgbouncer

if [[ -z $DB_HOST ]]; then
  echo "Use configuration from /etc/pgbouncer/pgbouncer.ini"
else
  printf "\
[databases]
* = host=${DB_HOST} \
port=${DB_PORT:-5432} user=${DB_USER:-postgres} \
${DB_PASSWORD:+password=${DB_PASSWORD}}

[pgbouncer]
logfile = ${PG_LOG}/pgbouncer.log

listen_addr = ${LISTEN_ADDR:-0.0.0.0}
listen_port = ${LISTEN_PORT:-5432}

${UNIX_SOCKET_DIR:+unix_socket_dir = ${UNIX_SOCKET_DIR}\n}\
${UNIX_SOCKET_DIR:+unix_socket_mode = ${UNIX_SOCKET_MODE:-0777}\n}\
${UNIX_SOCKET_DIR:+unix_socket_group = ${UNIX_SOCKET_GROUP:-postgres}\n}\

auth_type = ${AUTH_TYPE:-any}
${AUTH_FILE:+auth_file = ${AUTH_FILE}\n}\

${POOL_MODE:+pool_mode = ${POOL_MODE}\n}\

server_reset_query = ${SERVER_RESET_QUERY:-DISCARD ALL}

max_client_conn = ${MAX_CLIENT_CONN:-100}

default_pool_size = ${DEFAULT_POOL_SIZE:-20}
" > /etc/pgbouncer/pgbouncer.ini
fi

adduser postgres
mkdir -p ${PG_LOG}
chmod -R 755 ${PG_LOG}
chown -R postgres:postgres ${PG_LOG}

echo "Starting pgbouncer..."
pgbouncer ${QUIET:+-q} -u postgres /etc/pgbouncer/pgbouncer.ini
