FROM alpine:latest

LABEL maintainer="nixxlab@gmail.com"

ADD entrypoint.sh /
RUN chmod +x /entrypoint.sh && apk --no-cache --update add pgbouncer

ENTRYPOINT ["./entrypoint.sh"]