# PGBouncer

Docker image based on `alpine:latest`.

## Usage

```
docker run -d \
  --name=pgbouncer \
  -e DB_HOST=postgresql.example.com \
  -e DB_USER=admin \
  -e DB_PASSWORD=mypassword \
  nixxlab/pgbouncer:latest
```

Full list of environment variables you can see in `entrypoint.sh`.
